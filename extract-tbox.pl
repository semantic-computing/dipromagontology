#!/usr/bin/perl -w
use strict;
#use YAML qw(Dump);

# Usage: cat data.ntriples | perl extract_tbox.pl
# The files tbox.ttl and abox.ttl will be created.

# Note: This script was developed with the DiProMag ontology in mind. It might not correctly extract the TBox from other ontologies. 

# Author: Basil Ell

# Date: 2024-01-17

# License: CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/legalcode)

my @objects = ();
my $classes = {};

my $CFG->{prefix_definitions} = {
	"http://qudt.org/vocab/"							=> "qudt",
	"http://tpl.ottr.xyz/rdfs/0.2/" 						=> "o-rdfs",
	"http://www.w3.org/2002/07/owl#" 						=> "owl",
	"https://w3id.org/pmd/co/" 							=> "pmd",
	"http://www.w3.org/2001/XMLSchema#" 						=> "xsd",
	"http://www.w3.org/2000/01/rdf-schema#" 					=> "rdfs",
	"http://purl.org/linked-data/cube#" 						=> "qb",
	"http://tpl.ottr.xyz/p/docttr/0.1/" 						=> "o-docttr",
	"http://purl.org/dc/terms/" 							=> "terms",
	"http://ns.ottr.xyz/0.4/" 							=> "ottr",
	"http://purl.org/linked-data/sdmx/2009/concept#" 				=> "sdmx-concept",
	"http://tpl.ottr.xyz/owl/macro/0.1/" 						=> "o-owl-ma",
	"http://dublincore.org/specifications/dublin-core/dcmi-terms/2020-01-20/" 	=> "dcterms",
	"http://www.w3.org/ns/dcat#" 							=> "dcat",
	"http://www.w3.org/ns/prov-o#" 							=> "prov",
	"http://xmlns.com/foaf/0.1/" 							=> "foaf",
	"http://purl.org/linked-data/sdmx/2009/dimension#" 				=> "sdmx-dimension",
	"https://www.dipromag.de/dipromag_onto/0.1/" 					=> "dpm",
	"http://tpl.ottr.xyz/rdf/0.1/" 							=> "o-rdf",
	"http://www.w3.org/2002/07/emmo" 						=> "emmo",
	"http://purl.org/linked-data/sdmx/2009/measure#" 				=> "sdmx-measure",
	"http://www.w3.org/1999/02/22-rdf-syntax-ns#" 					=> "rdf",
	"http://reference.data.gov.uk/def/intervals/" 					=> "interval",
	"http://tpl.ottr.xyz/owl/axiom/0.1/" 						=> "o-owl-ax",
};

my $used_prefix = {};

# step 1: identify classes and properties
my $properties = {};
foreach my $line (<STDIN>){

	my $obj = &parse_NT_into_obj($line);
	next if not defined $obj;
	push(@objects, $obj);

	$classes->{$obj->{s}->{rep}} = 1
		if $obj->{p}->{rep} eq "<http://www.w3.org/2000/01/rdf-schema#subClassOf>"
			or $obj->{p}->{rep} eq "<http://www.w3.org/2000/01/rdf-schema#domain>"
			or $obj->{p}->{rep} eq "<http://www.w3.org/2000/01/rdf-schema#range>";

	$classes->{$obj->{o}->{rep}} = 1
		if $obj->{p}->{rep} eq "<http://www.w3.org/2000/01/rdf-schema#subClassOf>"
			or $obj->{p}->{rep} eq "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>";

	if($obj->{p}->{rep} eq "<http://www.w3.org/2000/01/rdf-schema#subPropertyOf>"){
		$properties->{$obj->{s}->{rep}} = 1;
		$properties->{$obj->{o}->{rep}} = 1;
	}
}

#print Dump { classes => $classes, properties => $properties }; exit();

# step 2: distinguish abox and tbox statements
my ($abox, $tbox) = ({}, {});
foreach my $obj (@objects){
	if(
		(
		not exists $classes->{$obj->{s}->{rep}} 
			or
		$obj->{p}->{rep} eq "<http://www.w3.org/1999/02/22-rdf-syntax-ns#type>"
		)
			and
		(not exists $properties->{$obj->{s}->{rep}} and not exists $properties->{o}->{rep})
	){
		$abox->{join(" ", &shorten($obj->{s}->{rep}), &shorten($obj->{p}->{rep}), &shorten($obj->{o}->{rep})) . " ."} = 1;
	} else {
		$tbox->{join(" ", &shorten($obj->{s}->{rep}), &shorten($obj->{p}->{rep}), &shorten($obj->{o}->{rep})) . " ."} = 1;
	}
}

#print Dump { usedprefix => $used_prefix }; exit();

print "ABox:\n";
print "$_\n" foreach keys %{$abox};

print "\nTBox:\n";
print "$_\n" foreach keys %{$tbox};

open(OUT,">tbox.ttl");
foreach my $part (keys %{$CFG->{prefix_definitions}}){
	my $name = $CFG->{prefix_definitions}->{$part};
	next if not exists $used_prefix->{$name};	
	print OUT "\@prefix $name: <$part> .\n";
}
print OUT "$_\n" foreach keys %{$tbox};
close OUT;
#system("rapper -i turtle -o dot tbox.ttl > tbox.dot");
#system("dot -Tpdf -Kdot tbox.dot -O");

open(OUT,">abox.ttl");
foreach my $part (keys %{$CFG->{prefix_definitions}}){
	my $name = $CFG->{prefix_definitions}->{$part};
	next if not exists $used_prefix->{$name};
	print OUT "\@prefix $name: <$part> .\n";
}
print OUT "$_\n" foreach keys %{$abox};
close OUT;
#system("rapper -i turtle -o dot abox.ttl > abox.dot");
#system("dot -Tpdf -Kdot abox.dot -O");


sub parse_NT_into_obj {
	my $string = shift;

	my ($s, $p, $o);

	# read subject
	
	if($string =~ m/(\A<[^<>]+>) /){
		$s = {
			type => "uri",
			rep => $1,
		};
		$string =~ s/$1\s*//;
	} elsif($string =~ m/\A(_:[^<>]+) /){
		$s = {
			type => "node",
			rep => $1,
		};
		$string =~ s/$1\s*//;
	} else {
		print "unexpected subject at the beginning of $string\n";
		return undef;
	}

	# read predicate
	if($string =~ m/(\A<[^<>]+>) /){
		$p = {
			type => "uri",
			rep => $1,
		};
		$string =~ s/$1\s*//;
	} else {
		print "unexpected predicate at the beginning of $string\n";
		return undef;
	}

	# read object
	if($string =~ m/\A(<[^<>]+>) \.\n\Z/){
		$o = {
			type => "uri",
			rep => $1,
		};
	} elsif($string =~ m/\A(_:[^<>]+) \.\n\Z/){
		$o = {
			type => "node",
			rep => $1,
		};
	} elsif($string =~ m/\A(\"(.*)\"\@(.+)) \.\n\Z/){
		$o = {
			type => "literal",
			rep => $1,
		};
	} elsif($string =~ m/\A(\"(.+)\"\^\^<(.+)>) \.\n\Z/){
		$o = {
			type => "literal",
			rep => $1,
		};
	} elsif($string =~ m/\A(\"(.*)\") .\n\Z/){
		$o = {
			type => "literal",
			rep => $1,
		};
	} else {
		print "unexpected object at the beginning of $string\n";
		return undef;
	}

	return { s => $s, p => $p, o => $o };
}

sub shorten {
	my $term = shift;

	if($term =~ m/\A<http/){
		foreach my $part (keys %{$CFG->{prefix_definitions}}){
			my $part_qm = quotemeta $part;
			if($term =~ m/\A<$part_qm(.*)>\Z/){
				$used_prefix->{$CFG->{prefix_definitions}->{$part}} = 1;				
				return $CFG->{prefix_definitions}->{$part} . ":" . $1;
			}
		}
		return $term;
	} elsif($term =~ m/"(.*)\^\^(<.*>)"\Z/){
		my ($value, $datatype) = ($1, $2);
		foreach my $part (keys %{$CFG->{prefix_definitions}}){
			my $part_qm = quotemeta $part;
			if($datatype =~ m/\A<$part_qm(.*)>\Z/){
				$used_prefix->{$CFG->{prefix_definitions}->{$part}} = 1;				
				return "\"$value\"^^" . $CFG->{prefix_definitions}->{$part} . ":" . $1;
			}
		}
	        return $term;	
	} else {
		return "$term";
	}
}
