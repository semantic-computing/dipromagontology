@prefix dpm:	<https://www.dipromag.de/dipromag_onto/0.1/> .
@prefix terms: <http://purl.org/dc/terms/> .
@prefix ottr:	<http://ns.ottr.xyz/0.4/>.
@prefix xsd:	<http://www.w3.org/2001/XMLSchema#> .
@prefix o-owl-ax: <http://tpl.ottr.xyz/owl/axiom/0.1/> .


dpm:GoalsAndAmbitions(
	dpm:GoalsAndAmbitionsTS20230606a, 
	("first-order magnetic transition near room-temperature"), 
	("enlarge change of magnetization when encountering small temperature change -> large magnetocaloric effect -> transfer more thermal energy from heating to cooling -> cooling application like refrigerator"), 
	("Elemental substitution"), 
	(dpm:CompositionModificationTS20230606a), 
	("from the substitution of Co for Ni you can expect higher magnetization", "A small amount of Al substitution for Ge can lower the structural transition, resulting in a coupled first-order magnetostructural transition has been realized in MnNi1-xCoxGe0.97Al0.03"),
	(), 
	("Nizioł S, Bombik A, Bażela W, Szytuła A and Fruchart D 1982 Magnetic phase diagram of CoxNi1-xMnGe Solid State Commun. 42 79 https://doi.org/10.1016/0038-1098(82)90356-8", "Tapas Samanta, Igor Dubenko, Abdiel Quetz, Samuel Temple, Shane Stadler, Naushad Ali; Magnetostructural phase transitions and magnetocaloric effects in MnNiGe1−xAlx. Appl. Phys. Lett. 2012; 100 052404. https://doi.org/10.1063/1.3681798"), 
	(), 
	("Dan’kov S Y, Tishin A M, Pecharsky V K and Gschneidner K A 1998 Magnetic phase transitions and the magnetothermal properties of gadolinium Phys. Rev. B 57 3478 https://doi.org/10.1103/PhysRevB.57.3478"), 
	(dpm:MnNi0.6Co0.4Ge0.97Al0.03, dpm:MnNi0.8Co0.2Ge0.97Al0.03)
) .
	
dpm:CompositionModification(
	dpm:CompositionModificationTS20230606a,
	dpm:MnNiGe,
	(dpm:Ni, dpm:Ge), 
	("20.00"^^xsd:float, "32.33"^^xsd:float), 
	(dpm:Co, dpm:Al), 
	("13.33"^^xsd:float, "1.00"^^xsd:float),
	dpm:MnNi0.6Co0.4Ge0.97Al0.03,
	""^^xsd:string
) .

dpm:CompositionModification(
	dpm:CompositionModificationTS20230606b,
	dpm:MnNiGe,
	(dpm:Ni, dpm:Ge), 
	("26.67"^^xsd:float, "32.33"^^xsd:float), 
	(dpm:Co, dpm:Al), 
	("6.67"^^xsd:float, "1.00"^^xsd:float),
	dpm:MnNi0.8Co0.2Ge0.97Al0.03,
	""^^xsd:string
) .

dpm:Composition(dpm:MnNi0.6Co0.4Ge0.97Al0.03, (dpm:Mn, dpm:Ni, dpm:Co, dpm:Ge, dpm:Al),("12"^^xsd:float, "0.6"^^xsd:float,"0.4"^^xsd:float, "0.97"^^xsd:float, "0.03"^^xsd:float)) .

dpm:Composition(dpm:MnNi0.8Co0.2Ge0.97Al0.03, (dpm:Mn, dpm:Ni, dpm:Co, dpm:Ge, dpm:Al),("1"^^xsd:float, "0.8"^^xsd:float,"0.2"^^xsd:float, "0.97"^^xsd:float, "0.03"^^xsd:float)) .

dpm:SampleSynthesisBulkArcMelt(
	dpm:SynthesisArcMeltBulk-BulkSampleTS2022-11-03-a-a, 
	"0.003"^^xsd:float, 
	"0.002998"^^xsd:float, 
	"20000"^^xsd:float, 
	dpm:Ar, 
	"50"^^xsd:float,
	"3"^^xsd:int,
	"Cu"^^xsd:string) .

dpm:SampleSynthesisBulkArcMelt(
	dpm:SynthesisArcMeltBulk-BulkSampleTS2022-11-03-a-b, 
	"0.003"^^xsd:float, 
	"0.0029781"^^xsd:float, 
	"20000"^^xsd:float, 
	dpm:Ar, 
	"50"^^xsd:float, 
	"3"^^xsd:int, 
	"Cu"^^xsd:string) .


# --- x2 ---
dpm:SampleProductionTreatedSample(dpm:BulkSampleTS2022-11-03-a-AM-a, dpm:BulkSampleTS2022-11-03-a, dpm:TreatmentAnnealBulksample-MnNi0.6Co0.4Ge0.97Al0.03-1173-a) . 

dpm:SampleTreatmentBulkAnneal(dpm:TreatmentAnnealBulksample-MnNi0.6Co0.4Ge0.97Al0.03-1173-a, dpm:Ar, "20000"^^xsd:float, ("100"^^xsd:float), ("200"^^xsd:float), ("5"^^xsd:float), "0.00216181"^^xsd:float, "0.00216060"^^xsd:float, dpm:Quartz, "1173"^^xsd:float, ""^^xsd:string) .

dpm:MeasurementMagneticProperties(dpm:MeasurementMagneticProperties-BulkSampleTS2022-11-03-a-AM-a-a, dpm:MPMS, ottr:none, dpm:BulkSampleTS2022-11-03-a-AM-a, "0.0000069"^^xsd:float, ottr:none, dpm:piece, ottr:none, ""^^xsd:string, dpm:straw, "0"^^xsd:float, ottr:none, "false"^^xsd:boolean) .

dpm:MeasurementMagneticPropertiesIsofield(dpm:MeasurementIsofieldMagneticPropertiesMeasurement-BulkSampleTS2022-11-03-a-AM-a-a-a, dpm:MeasurementMagneticProperties-BulkSampleTS2022-11-03a-AM-a-a, (("7"^^xsd:float, "5.5"^^xsd:float, "0.5"^^xsd:float), ("5"^^xsd:float, "2"^^xsd:float, "0.2"^^xsd:float), ("1.9"^^xsd:float, "0.1"^^xsd:float, "0.1"^^xsd:float), ("0.01"^^xsd:float, "0.01"^^xsd:float, "0"^^xsd:float)), (("220"^^xsd:float, "280"^^xsd:float, "1"^^xsd:float)), ("2"^^xsd:float), "1"^^xsd:float) .

dpm:MeasurementMagneticPropertiesTransitionCharacteristics(dpm:MeasurementIsofieldMagneticPropertiesMeasurement-BulkSampleTS2022-11-03-a-AM-a-a-a, ("0.2"^^xsd:float, "2"^^xsd:float, "5"^^xsd:float, "7"^^xsd:float), ("290"^^xsd:float, "291"^^xsd:float, "293"^^xsd:float, "293.9"^^xsd:float), ("285.7"^^xsd:float, "286.7"^^xsd:float, "288.5"^^xsd:float, "290.3"^^xsd:float), ("4.3"^^xsd:float, "4.3"^^xsd:float, "4.5"^^xsd:float, "3.6"^^xsd:float)) .
	
dpm:MeasurementMagneticPropertiesEntropyChange(dpm:MeasurementMagneticProperties-BulkSampleTS2022-11-03-a-AM-a-a-a, ("1"^^xsd:float, "2"^^xsd:float, "5"^^xsd:float, "7"^^xsd:float), ("290.3"^^xsd:float, "290.3"^^xsd:float, "291.2"^^xsd:float, "292.2"^^xsd:float), ("285.4"^^xsd:float, "286.4"^^xsd:float, "287.3"^^xsd:float, "288.3"^^xsd:float), ("0.67"^^xsd:float, "2.83"^^xsd:float, "19.05"^^xsd:float, "29.35"^^xsd:float), ("0.53"^^xsd:float, "2.36"^^xsd:float, "16.39"^^xsd:float, "25.35"^^xsd:float), ottr:none, ("0.42"^^xsd:float, "1.80"^^xsd:float, "12.58"^^xsd:float, "20.78"^^xsd:float)) .


# --- x4 ---
dpm:SampleProductionTreatedSample(dpm:BulkSampleTS2022-11-03-b-AM-a, dpm:BulkSampleTS2022-11-03-b, dpm:TreatmentAnnealBulksample-MnNi0.8Co0.2Ge0.97Al0.03-1173-a) . 

dpm:SampleTreatmentBulkAnneal(dpm:TreatmentAnnealBulksample-MnNi0.8Co0.2Ge0.97Al0.03-1173-b, dpm:Ar, "20000"^^xsd:float, ("100"^^xsd:float), ("200"^^xsd:float), ("5"^^xsd:float), "0.00216181"^^xsd:float, "0.00216060"^^xsd:float, dpm:Quartz, "1173"^^xsd:float, ""^^xsd:string) .

dpm:MeasurementMagneticProperties(dpm:MeasurementMagneticProperties-BulkSampleTS2022-11-03-b-AM-a-a, dpm:MPMS, ottr:none, dpm:BulkSampleTS2022-11-03-b-AM-a, "0.00000857"^^xsd:float, ottr:none, dpm:Piece, ottr:none, ""^^xsd:string, dpm:Straw, "0"^^xsd:float, ottr:none, "false"^^xsd:boolean) .

dpm:MeasurementMagneticPropertiesIsofield(dpm:MeasurementIsofieldMagneticPropertiesMeasurement-BulkSampleTS2022-11-03-b-AM-a-a-a, dpm:MeasurementMagneticProperties-BulkSampleTS2022-11-03a-AM-a-b, (("7"^^xsd:float, "5.5"^^xsd:float, "0.5"^^xsd:float), ("5"^^xsd:float, "2"^^xsd:float, "0.2"^^xsd:float), ("1.9"^^xsd:float, "0.1"^^xsd:float, "0.1"^^xsd:float), ("0.01"^^xsd:float, "0.01"^^xsd:float, "0"^^xsd:float)), (("260"^^xsd:float, "340"^^xsd:float, "1"^^xsd:float)), ("2"^^xsd:float), "1"^^xsd:float) .
	
dpm:MeasurementMagneticPropertiesTransitionCharacteristics(dpm:MeasurementIsofieldMagneticPropertiesMeasurement-BulkSampleTS2022-11-03-b-AM-a-a-a, ("0.2"^^xsd:float, "2"^^xsd:float, "5"^^xsd:float, "7"^^xsd:float), ("251.5"^^xsd:float, "251.5"^^xsd:float, "252.5"^^xsd:float, "254.4"^^xsd:float), ("247.5"^^xsd:float, "248.5"^^xsd:float, "249.5"^^xsd:float, "151.5"^^xsd:float), ("3.9"^^xsd:float, "2.9"^^xsd:float, "2.9"^^xsd:float, "2.9"^^xsd:float)) .

dpm:MeasurementMagneticPropertiesEntropyChange(dpm:MeasurementIsofieldMagneticPropertiesMeasurement-BulkSampleTS2022-11-03-b-AM-a-a-a, ("1"^^xsd:float, "2"^^xsd:float, "5"^^xsd:float, "7"^^xsd:float), ("251.3"^^xsd:float, "251.5"^^xsd:float, "251.5"^^xsd:float, "251.5"^^xsd:float), ("247.5"^^xsd:float, "247.5"^^xsd:float, "248.5"^^xsd:float, "249.5"^^xsd:float), ("3.24"^^xsd:float, "6.72"^^xsd:float, "16.39"^^xsd:float, "22.42"^^xsd:float), ("2.99"^^xsd:float, "6.49"^^xsd:float, "16.11"^^xsd:float, "21.78"^^xsd:float), ottr:none, ("2.85"^^xsd:float, "6.07"^^xsd:float, "14.89"^^xsd:float, "20.27"^^xsd:float)) .

dpm:MeasurementXRD(dpm:BulkSampleTS2022-11-03-a-AM-a, dpm:powder, (("300"^^xsd:float, "300"^^xsd:float, "0"^^xsd:float)), "20"^^xsd:float, "70"^^xsd:float, "0.05"^^xsd:float, "0.5"^^xsd:float, "1.5406"^^xsd:float, dpm:Continuous, ("300"^^xsd:float), ("1"^^xsd:int), ("P63/mmc"^^xsd:string), (("4.08801"^^xsd:float, "4.08801"^^xsd:float, "5.37937"^^xsd:float)), (("90"^^xsd:float, "90"^^xsd:float, "120"^^xsd:float)), ("78.4376"^^xsd:float)) .
	
	
	
	
	
	