# Requirements and Competency Questions 


1. **Which domain should be covered by the ontology?** 

Experiments with Magnetocaloric materials: Bulk, Thin-Films, Simulations, Applications.

2. **What should the ontology be used for?** 

The ontology should store the most important experiment data (outcomes, observations, dependencies, problems) which is also published in scientific literature or needed for reproducibility. This is a very short overview of the data:
* compound stoichiometry (and dependencies to other compounds)
* production parameters of new compounds or samples: melting, sputtering, 3D printing, ...
* crystallography (CIF representation)
* settings and outcomes of measurement methods and simulations: MPMS, XRD characterization, TEM-FIB, ...
This includes that some simple processes, e.g. material preparation, are modelled, too.
Furthermore, goals, incentives and expectations of the experiments should be traceable.

Based on this, we plan to have different applications beyond simple data storing:
* the data should be used to generate plots which are interesting (show trends in the laboratory or dependencies of parameters) for the domain experts, e.g., plot experiment outcomes (\delta S max, T_c, ...) depending on the amount of a certain element in a considered compound or how much of a certain element is consumed by whom 
* a vector-space representation of the ontology should be learned, based on which dependencies or analogies can be found or recommendations for new experiments can be made

3. **Who will use the ontology?**

Domain Experts should be able to query data, generate plots and get recommendations. Therefore, a machine learning model (vector space embeddings, recommendation system) should access the data, as well. 

4. **What questions should be answered by the ontology?** (max 15 questions) 

	- How is the stoichiometry of a considered sample? E.g. sample x has stoichiometry Mn1.82Cu0.18Sb.
	- Which materials in which purity and shape are used? E.g. the element x, y,z are used with purities of 99% and in powder.
	- How much of each material is used, and how is this related to the stoichiometry (target mass, actual mass, mass deviation)? E.g. the stoichiometry says 1.82 of x, this would be a target mass of 1.08742g, but only 1.08737g are actually used, this is a deviation of -0.00005g.
	- What is the mass of the overall produced sample? E.g. overall sample weight is 2.40438g.
	- Which substitution or doping is applied to a source compound to create a new compound? E.g. substitution is applied by removing some portion of x and inserting y.
	- Is a compound off-stoichiometric? This would be Yes or No.
	- Which compounds are based on (with additional substitution or doping) a source compound? E.g. Mn096Co007Ge has the parent compound MnCoGe and is created through substitution.
	- What are the outcomes of measurement method x? E.g. MPMS provides \delta T hysteresis and maximum \delta S values. 
	- What is the value of property Y on compound Z? E.g. \delta S has the value 28 J/(kg*K) with an applied magnetic field of 1T and at a temperature of 308K during cooling.
	- What are the settings for a machine used to produce a measurement value? E.g. applied magnetic field, temperature, angles, holder, volume.
	- Which steps are performed in the produce of a compound? E.g. melting at temperature x, then ...
	- How depends the property Y on the amount of element Z in the compound? E.g. data to generate plot property x (e.g. \delta S max) depending on the concentration of y.
	- Which experiments/measurements are missing for a compound? E.g. \delta S max is not measured.
	- Which compounds are promising and should be produced and evaluated? E.g. compound x has archived good results when y is applied. Try to apply y to z. 
	- Which unobserved dependencies exist within the data? E.g. \delta S max depends on the concentration of x in compound y. 
	

 

